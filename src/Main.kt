import interactions.DialogueInteraction
import interactions.ExamineInteraction
import interactions.InteractionStrategy

fun main() {

    var person = Player()


    var dialogueBuilder = DialogueBuilder().add("Somebody once told me").add("The world was gonna roll me").add("I ain't the sharpest tool in the shed")

    var atm = ATM()
    var npc = NPC(mutableListOf<InteractionStrategy>(
            DialogueInteraction(dialogueBuilder.build()),
            ExamineInteraction("An NPC: Noob Player Character")
    ))

    person.interact(atm)
    //person.interact(npc)

}

