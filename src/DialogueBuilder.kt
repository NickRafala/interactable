class DialogueBuilder {
    var textList = mutableListOf<Node>()
    private var dialogue = Dialogue()

    fun add(text: String): DialogueBuilder {
        dialogue.dialogue.add(Node(text))
        return this
    }

    fun build() = dialogue

}