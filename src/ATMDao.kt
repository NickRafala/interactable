import java.sql.Connection
import java.sql.DriverManager
import java.util.*

class ATMDao {

    private var conn: Connection? = null

    init {
        var connectionProps = Properties()
        connectionProps.put("user", "root")
        connectionProps.put("password", "")
        Class.forName("com.mysql.jdbc.Driver").newInstance()

        try { // try to connect to DB
            conn = DriverManager.getConnection(
                    "jdbc:" + "mysql" + "://" +
                            "127.0.0.1" +
                            ":" + "3306" + "/" +
                            "",
                    connectionProps)
        } catch (ex: Throwable) {}

        if (conn?.isClosed == false) {
            try { // try to create DB
                var statement = conn!!.createStatement()
                statement.execute("CREATE DATABASE atm;")
            } catch (ex: Throwable) {}

            try { // try to create table and create default values
                var statement = conn!!.createStatement()
                statement.execute("USE atm;")
                statement.execute("CREATE TABLE atm (id int NOT NULL AUTO_INCREMENT, balance int, PRIMARY KEY(id));")
                statement.execute("INSERT INTO atm (balance) VALUES (0);")

            } catch (ex: Throwable) {}
        }
    }

    fun updateBalance(id: Int, bal: Int) {
        if (conn?.isClosed == false) {
            var statement = conn!!.createStatement()
            statement.execute("UPDATE atm SET balance = $bal WHERE id = $id;");
        }
    }

    fun getBalance(id: Int): Int {
        if (conn?.isClosed == false) {
            var statement = conn!!.createStatement()
            statement.executeQuery("USE atm;");
            var resultSet = statement.executeQuery("SELECT balance FROM atm WHERE id = $id;");

            if (resultSet.next())
                return resultSet.getInt("balance")
        }
        return 0
    }
}
