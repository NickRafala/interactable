import interactions.InteractionStrategy
import java.net.ConnectException
import java.sql.Connection
import java.sql.DriverManager
import java.sql.Statement
import java.util.*

class ATM(override var interactions: MutableList<InteractionStrategy> = mutableListOf<InteractionStrategy>()) : Interactable {
    private var id = 1
    private val dao: ATMDao = ATMDao()
    var cash = dao.getBalance(id)

    fun withdraw(player: Player) {

        println("Withdraw amount:")
        val input = readLine()!!.toInt()
        if (cash < input) {
            println("Not enough cash")
            return
        }

        cash -= input
        player.cash += input

        dao.updateBalance(id, cash)
    }

    fun deposit(player: Player) {
        println("Deposit amount:")
        val input = readLine()!!.toInt()
        cash += input
        player.cash -= input

        dao.updateBalance(id, cash)
    }

    fun printCash(player: Player) {
        println("Amount: $cash")
        println("Wallet: ${player.cash}")
    }

    override fun interact(user: Player) {
        // override function
        // no interaction strategies

        var input = 0
        do {
            println("1) Withdraw")
            println("2) Deposit")
            println("3) check amount")
            println("0) Quit")
            input = readLine()!!.toInt()

            when (input) {
                1 -> withdraw(user)
                2 -> deposit(user)
                3 -> printCash(user)
            }
        } while (input > 0)
    }


}