package interactions

import Dialogue
import Interactable
import Player

class DialogueInteraction (
        var dialogue: Dialogue, override var strategyName: String = "Dialogue"
) : InteractionStrategy {

    override fun execute(user: Player, interactable: Interactable) {
        dialogue.start()
    }
}
