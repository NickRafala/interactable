package interactions

import Interactable
import Player

interface InteractionStrategy {

    var strategyName: String

    fun execute(user: Player, interactable: Interactable)
}
