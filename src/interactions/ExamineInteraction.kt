package interactions

import Interactable
import NPC
import Player

class ExamineInteraction(var text: String, override var strategyName: String = "Examine") : InteractionStrategy {
    override fun execute(user: Player, interactable: Interactable) {
        println(text)
    }
}
