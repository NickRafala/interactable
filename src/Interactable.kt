import interactions.InteractionStrategy

interface Interactable {

    var interactions: MutableList<InteractionStrategy>

    fun interact(user: Player) {

        when (interactions.size) {
            0 -> return
            1 -> interactions[0].execute(user, this)
            else -> {
                println("Select an option:")
                for ((i, inter) in interactions.withIndex()) {
                    println("${i + 1}. ${inter.strategyName}")
                }

                val input = readLine()!!.toInt()
                interactions[input - 1].execute(user, this)
            }
        }


    }

}