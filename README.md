Languages: Kotlin, SQL

Summary:
Practice with using Strategy pattern. A Player can interact with Interactables such as an ATM and an NPC (non-player character). Strategy pattern is used to detemine how the player is able to interact with Interactables.

The ATM can also connect to a SQL database to store the amount of cash in the ATM. Defaults to 0 if it cannot connect to a database. A Data Access Object (DAO) is used to abstract any database transactions.